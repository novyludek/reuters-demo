from flask import Flask
from flask.json import JSONEncoder
from datetime import date
from blueprint.article import route_article
from flask_sqlalchemy import SQLAlchemy


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


def server_setup(config):
    app = Flask(__name__)
    app.json_encoder = CustomJSONEncoder
    app.config.from_object(config)
    app.register_blueprint(route_article, url_prefix='/api/v1')
    db = SQLAlchemy()
    db.init_app(app)

    return app

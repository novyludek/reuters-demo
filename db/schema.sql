CREATE EXTENSION "uuid-ossp";

CREATE SCHEMA IF NOT EXISTS articles;

CREATE TABLE articles.data(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    topic character varying(5),
    lewissplit character varying(15),
    cgisplit character varying(30),
    oldid integer,
    newid integer,
    places jsonb,
    mknote text,
    people jsonb,
    date date,
    orgs jsonb,
    topics text,
    exchanges jsonb,
    companies jsonb,
    unknown text,
    text_type character varying(10),
    body text,
    dateline text,
    title text,
    author text
);

COPY articles.data(topic,lewissplit,cgisplit,oldid,newid,places,mknote,people,date,orgs,topics,exchanges,companies,unknown,text_type,body,dateline,title,author) 
FROM '/tmp/psql_data/seed.csv' DELIMITER ',' CSV;
from bs4 import BeautifulSoup
import csv
import re
import json

f = open('dataset/reut2-000.sgm', 'r')
data = f.read()
soup = BeautifulSoup(data, "html.parser")

reuters_attributes = ['topics', 'lewissplit', 'cgisplit', 'oldid', 'newid']
text_attributes = ['type']

text_tags = {
    'author': "string",
    'title': "string",
    'dateline': "string",
    'body': "string"
}
reuter_tags = {
    'date': "string",
    'topics': "list",
    'mknote': "string",
    'places': "list",
    'people': "list",
    'orgs': "list",
    'exchanges': "list",
    'companies': "list",
    'unknown': "string"
}

contents = soup.findAll('reuters')


def sgm_parser(input_data):
    csv_data = []
    for content in input_data:
        data = []

        # reuters attributes
        for attr in reuters_attributes:
            data.append(content.attrs[attr])

        # reuters tags
        for tag in reuter_tags:
            value = content.find(tag)
            tag_type = reuter_tags[tag]
            data_to_insert = data_fetcher(value, tag_type)
            data.append(data_to_insert)

        # text attributes
        for text_attribute in text_attributes:
            text_tag = content.find('text')
            try:
                text_tag_attr_value = text_tag.attrs[text_attribute]
                data.append(text_tag_attr_value)
            except BaseException:
                # default value
                data.append("NORM")

        # text tags
        for text_tag in text_tags:
            text_tag_value = content.find(text_tag)
            text_tag_data = data_fetcher(text_tag_value, tag_type)
            data.append(text_tag_data)

        csv_data.append(data)

    with open("output.csv", "wb") as f:
        writer = csv.writer(f)
        writer.writerows(csv_data)


def data_fetcher(value, tag_type):
    if (value is not None):
        if (tag_type == "string"):
            value_text = str(value.text)
            return string_normalization(value_text)
        elif (tag_type == "list"):
            value_list = list_normalization(value.findAll("d"))
            return json.dumps(value_list)
    else:
        return empty_value(tag_type)


def list_normalization(input_data):
    return [str(value.text) for value in input_data]


def string_normalization(input_string):
    strip_newline = input_string.replace('\n', '')
    string_no_spaces = re.sub(' +', ' ', strip_newline)
    return string_no_spaces


def empty_value(tag_type):
    if (tag_type == "string"):
        return ""
    elif (tag_type == "list"):
        return json.dumps("[]")


sgm_parser(contents)

import pytest
from endpoints import article_detail, articles_listing


@pytest.fixture(scope="session")
def article_id(request):
    r = articles_listing({"limit": 1})
    return r.json()["articleListing"][0]["id"]


def test_fetch_article_id(article_id):
    r = article_detail(article_id)
    article_detail_data = r.json()
    assert r.status_code == 200
    assert article_detail_data["id"] == article_id


def test_unexisting_article_id():
    r = article_detail("59bf37d8-c975-4e98-b634-36943867bd75")
    assert r.status_code == 404


def test_invalid_uuid():
    r = article_detail("9bf37d8-c975-4e98-b634")
    assert r.status_code == 400

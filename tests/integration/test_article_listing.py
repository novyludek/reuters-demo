from endpoints import articles_listing


def test_it_fetches_data():
    r = articles_listing()
    assert r.status_code == 200
    listing = r.json()["articleListing"]
    total = r.json()["total"]
    for item in listing:
        assert item["id"] is not None
    assert len(listing) == 300, "default listing limit should be 300"
    assert isinstance(total, int), "total should be type of int"


def test_limit_data():
    r = articles_listing({"limit": 1})
    assert r.status_code == 200
    assert len(r.json()["articleListing"]
               ) == 1, "should return only one result"


def test_exceed_limit_data():
    r = articles_listing({"limit": 301})
    assert r.status_code == 400


def test_offset_data():
    first_two_results = articles_listing({"limit": 2})
    offset_results = articles_listing({"offset": 2, "limit": 10})
    first_two_ids = [r["id"]
                     for r in first_two_results.json()["articleListing"]]
    offset_ids = [r["id"] for r in offset_results.json()["articleListing"]]
    for id in first_two_ids:
        assert id not in offset_ids, "first two results shoud not be present in offset results"


def test_invalid_offset_type():
    r = articles_listing({"offset": "abc"})
    assert r.status_code == 400


def test_invalid_limit_type():
    r = articles_listing({"limit": "abc"})
    assert r.status_code == 400

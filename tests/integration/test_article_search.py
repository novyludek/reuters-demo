# -*- coding: utf-8 -*-
import pytest
from endpoints import article_search
from datetime import datetime


@pytest.mark.parametrize("test_input", [
    {"body": "grain"},
    {"title": "usa"},
    {"author": "david"},
    {"newid": 1, },
    {"oldid": 12518},
    {"places": ["usa"]},
    {"places": ["usa", "japan"]},
    {"people": ["reagan"]},
    {"people": ["reagan", "ongpin"]},
    {"topics": ["wheat"]},
    {"topics": ["wheat", "livestock"]},
    {"orgs": ["opec"]},
    {"orgs": ["opec", "ec"]},
    {"exchanges": ["cme"]},
    {"exchanges": ["cme", "amex"]},
    {"topic": "YES"},
    {"topic": "NO"},
    {"day": 2},
    {"month": 3},
    {"year": 1987},
    {"year": 1987, "places": ["usa"]},
    {"month": 0o3, "people": ["reagan"]},
    {"topics": ["gas"], "exchanges": ["nymex"]},
    {"places": ["spain"], "people": ["yeutter"], "orgs": ["gatt"], "topics": ["oilseed"]}
])
def test_it_returns_data_when_valid_filter_provided(test_input):
    r = article_search(dict(test_input))
    assert r.status_code == 200
    results = r.json()["results"]
    assert len(results) > 0, "returns results for input %s" % test_input
    for result in results:
        for key in test_input:
            if (key in ["year", "month", "day"]):
                datetime_object = datetime.strptime(result["date"], "%Y-%m-%d")
                if (key == "year"):
                    assert datetime_object.year == test_input[key]
                elif(key == "month"):
                    assert datetime_object.month == test_input[key]
                elif(key == "day"):
                    assert datetime_object.day == test_input[key]
            if (isinstance(test_input[key], 'str')):
                assert test_input[key].lower() in result[key].lower()
            elif (isinstance(test_input[key], "list")):
                assert any(x in result[key] for x in test_input[key])


@pytest.mark.parametrize("test_input", [
    ({"body": 1}),
    ({"title": ["usa"]}),
    ({"author": None}),
    ({"newid": "1"}),
    ({"newid": 1.1}),
    ({"oldid": [1]}),
    ({"places": "usa"}),
    ({"people": [1]}),
    ({"topics": [1]}),
    ({"orgs": "opec"}),
    ({"orgs": None}),
    ({"exchanges": "cme"}),
    ({"exchanges": [1.1]}),
    ({"topic": "true"}),
    ({"topic": "false"}),
    ({"day": "2"}),
    ({"day": -2}),
    ({"month": [3]}),
    ({"year": None}),
    ({"year": 1.1})
])
def test_it_returns_400_when_invalid_data_type_provided(test_input):
    r = article_search(test_input)
    assert r.status_code == 400


@pytest.mark.parametrize("test_input", [
    '!@#$%^&*()`~',
    '사회과학원 어학연구소',
    '0.0/0.0',
    'Ω≈ç√∫˜µ≤≥÷',
    'åß∂ƒ©˙∆˚¬…æ',
    'œ∑´®†¥¨ˆøπ“‘',
    '¡™£¢∞§¶•ªº–≠',
    '¸˛Ç◊ı˜Â¯˘¿',
])
def test_ugly_strings(test_input):
    r = article_search(dict({"body": test_input}))
    assert r.status_code == 200
    assert len(r.json()["results"]) == 0, "should not return any results"


@pytest.mark.parametrize("test_input", [
    '-- or #',
    '" OR "1'
    '" OR "" = "',
    "'",
    ";"
])
def trivial_sql_injection(test_input):
    r = article_search(dict({"body": test_input}))
    assert r.status_code == 200
    assert len(r.json()["results"]) == 0, "should not return any results"


def test_it_returns_no_results_when_empty_body():
    r = article_search(dict({}))
    assert r.status_code == 400


def test_it_returns_no_results_when_unexisting_filter_used():
    r = article_search(dict({"test": "abcd"}))
    assert r.status_code == 400

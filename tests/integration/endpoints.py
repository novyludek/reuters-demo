import requests


def _url(path):
    return 'http://be:5099/api/v1' + path


def articles_listing(params=None):
    return requests.get(_url('/articles'), params)


def article_detail(article_id):
    return requests.get(_url('/articles/{:s}'.format(article_id)))


def article_search(json_body):
    return requests.post(_url('/articles/search'), json=json_body)

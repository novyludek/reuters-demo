FROM python:2.7.16-alpine3.9

WORKDIR /src

COPY requirements.txt .

RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
 python -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

RUN python -m pip install -r requirements.txt --no-cache-dir

COPY . .

CMD [ "python", "app.py" ]

from blueprint.validators.article_body_schema import ArticleSearchBody
import json
from flask import Blueprint, request, jsonify, abort
from utils.postgres import db
from blueprint.queries.article_queries import get_total_count, get_articles_data, get_article_detail, filter_articles
from blueprint.validators.article_param_schema import ArticleQueryInput, ArticlesQueryInputs

route_article = Blueprint('route_article', __name__)


@route_article.route('/articles/<article_id>', methods=['GET'])
def get_article(article_id):
    inputs = ArticleQueryInput(request)
    if not inputs.validate():
        return jsonify(success=False, errors=inputs.errors), 400
    article_detail_data = db.engine.execute(
        get_article_detail(), {
            'id': article_id}).fetchall()
    if len(article_detail_data) == 0:
        return abort(404)
    return jsonify(rows_to_dict(article_detail_data)[0])


@route_article.route('/articles', methods=['GET'])
def get_article_listing():
    limit = request.args.get("limit", 300)
    offset = request.args.get('offset', 0)
    inputs = ArticlesQueryInputs(request)
    if not inputs.validate():
        return jsonify(success=False, errors=inputs.errors), 400

    total_count = db.engine.execute(get_total_count()).fetchone()
    results = db.engine.execute(
        get_articles_data(), {
            'limit': limit, 'offset': offset}).fetchall()
    response_data = dict(
        {'total': total_count[0], 'articleListing': rows_to_dict(results)})
    return jsonify(response_data)


@route_article.route('/articles/search', methods=['POST'])
def search_articles():
    body_validator = ArticleSearchBody(request)
    if not body_validator.validate():
        return jsonify(success=False, errors=body_validator.errors), 400
    query_filters = {}
    fulltext_string_filters = ["body", "dateline", "title", "author"]
    filters = [
        "people",
        "places",
        "companies",
        "topics",
        "orgs",
        "exchanges",
        "topic",
        "newid",
        "oldid",
        'author',
        "day",
        "month",
        "year"]
    all_filters = fulltext_string_filters + filters
    input_body_data = json.loads(request.data)

    for f in all_filters:
        if f in input_body_data:
            if f in fulltext_string_filters:
                like_param = "%" + input_body_data[f] + "%"
                query_filters.update({f: like_param})
            else:
                query_filters.update({f: input_body_data[f]})
        else:
            query_filters.update({f: None})
    result_search = db.engine.execute(
        filter_articles(), query_filters).fetchall()
    return jsonify({"numberOfResults": len(result_search),
                    "results": rows_to_dict(result_search)})


def rows_to_dict(query_rows):
    return [(dict(row.items())) for row in query_rows]

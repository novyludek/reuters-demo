from flask_inputs import Inputs
from flask_inputs.validators import JsonSchema

schema = {
    'type': 'object',
    'minProperties': 1,
    'additionalProperties': False,
    'properties': {
        'body': {'type': 'string', "minLength": 1},
        'dateline': {'type': 'string', "minLength": 1},
        'title': {'type': 'string', "minLength": 1},
        'newid': {'type': 'integer', "minimum": 0},
        'oldid': {'type': 'integer', "minimum": 0},
        'people': {
            'type': 'array',
            'items': {
                'type': "string"
            }},
        'places': {
            'type': 'array',
            'items': {
                'type': "string"
            }},
        'companies': {
            'type': 'array',
            'items': {
                'type': "string"
            }},
        'topics': {
            'type': 'array',
            'items': {
                'type': "string"
            }},
        'orgs': {
            'type': 'array',
            'items': {
                'type': "string"
            }},
        'exchanges': {
            'type': 'array',
            'items': {
                'type': "string"
            }},
        'topic': {
            "type": "string",
            "enum": ["YES", "NO"]
        },
        'author': {'type': 'string', "minLength": 1},
        'year': {'type': 'integer', "minimum": 0},
        'month': {'type': 'integer', "minimum": 0},
        'day': {'type': 'integer', "minimum": 0}
    }
}


class ArticleSearchBody(Inputs):
    json = [JsonSchema(schema=schema)]

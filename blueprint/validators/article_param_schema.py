from flask_inputs import Inputs
from wtforms.validators import DataRequired, UUID, Optional
from wtforms import ValidationError


def limit_range(forms, field):
    size = int_cast(field.data, field.name)
    if not 0 <= size <= 300:
        raise ValidationError('Limit is not within 0 and 300.')


def int_type(forms, field):
    int_cast(field.data, field.name)


def int_cast(inpt, name):
    try:
        return int(inpt)
    except BaseException:
        raise ValidationError('%s: not a valid number.' % name)


class ArticleQueryInput(Inputs):
    rule = {
        'article_id': [DataRequired(), UUID()]
    }


class ArticlesQueryInputs(Inputs):
    args = {
        'limit': [Optional(), limit_range],
        'offset': [Optional(), int_type]
    }

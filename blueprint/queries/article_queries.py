from sqlalchemy.sql import text


def get_articles_data():
    return text(
        "SELECT id, title FROM articles.data LIMIT :limit OFFSET :offset")


def get_total_count():
    return text("SELECT count(*) FROM articles.data")


def get_article_detail():
    return text("SELECT * FROM articles.data WHERE id = :id")


def filter_articles():
    return text(
        '''
  SELECT id, title, places, people, topics, companies, orgs, exchanges, body, title, dateline,
  author, topic, newid, oldid, date FROM articles.data as data
  WHERE (:places IS NULL OR data.places::jsonb ?| array[:places])
  AND (:people IS NULL OR data.people::jsonb ?| array[:people])
  AND (:topics IS NULL OR data.topics::jsonb ?| array[:topics])
  AND (:companies IS NULL OR data.companies::jsonb ?| array[:companies])
  AND (:orgs IS NULL OR data.orgs::jsonb ?| array[:orgs])
  AND (:exchanges IS NULL OR exchanges::jsonb ?| array[:exchanges])
  AND (:body IS NULL OR LOWER(data.body) LIKE LOWER(:body))
  AND (:title IS NULL OR LOWER(data.title) LIKE LOWER(:title))
  AND (:dateline IS NULL OR LOWER(data.dateline) LIKE LOWER(:dateline))
  AND (:author IS NULL OR LOWER(data.author) LIKE LOWER(:author))
  AND (:topic IS NULL OR LOWER(data.topic) = LOWER(:topic))
  AND (:newid IS NULL OR data.newid = :newid)
  AND (:oldid IS NULL OR data.oldid = :oldid)
  AND (:year IS NULL OR EXTRACT(YEAR FROM data.date) = :year)
  AND (:month IS NULL OR EXTRACT(MONTH FROM data.date) = :month)
  AND (:day IS NULL OR EXTRACT(DAY FROM data.date) = :day)
  ''')

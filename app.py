import os
from utils.config import ProductionConfig, DevelopmentConfig
from utils.server_setup import server_setup

if __name__ == '__main__':
    if os.environ.get('ENVIRONMENT') == 'PROD':
        app = server_setup(ProductionConfig)
        app.run(port=5099, host="0.0.0.0", use_reloader=False)
    else:
        app = server_setup(DevelopmentConfig)
        app.run(port=5099, host="0.0.0.0", use_reloader=True)
